INSTAPHOTO

Per a l'execuci� del projecte, llegir el fitxer RUN.txt

Una vegada executant el projecte, cal entrar a localhost:3000

A l'arrel del projecte hi ha un v�deo de demostraci� del funcionament del projecte.


PANTALLA DE LOGIN:
-----------------------------------------------------------------------------------------------

La pantalla de login permet entrar amb un usuari ja existent o b� entrar a crear un nou usuari, amb Sign Up here.

Podem entrar amb tres usuaris predefinits (o amb un de nou):

Usuari: jo
Contrasenya: jo

L'usuari jo segueix test1 i test2. T� 6 posts propis, el primer amb un like.

Usuari: test1
Contrasenya: test1

L'usuari test1 no segueix a ning�. T� un post propi.

Usuari: test2
Contrasenya: test2

L'usuari test2 segueix a jo. T� un post propi.

La creaci� d'usuaris permet escollir una foto de perfil i totes les dades t�piques d'un formulari d'alta.

Una vegada dins, es pot veure la barra de navegaci� amb les icones de HOME, XAT, NOU POST, PERFIL, CERCA i LOGOFF

HOME:
-----------------------------------------------------------------------------------------------
Aqu� podem veure tots els posts dels usuaris als que seguim, no hi surten els posts propis. Fent clic a un post, podem veure'l m�s gran, fer-hi like,
veure quins usuaris han fet like al post i afegir comentaris. Tot l'apartat de la galeria d'imatges i el detall dels post �s responsive.

XAT:
-----------------------------------------------------------------------------------------------
Podem veure quins usuaris estan en l�nia, i fer un xat conjunt amb tots els usuaris o b� directe amb un usuari en privat. Es mostren les notificacions
de nous missatges a la part esquerra de la pantalla, aix� com els usuaris que estan en l�nia. Cada usuari surt amb un color diferent i la seva icona de perfil.


NOU POST:
-----------------------------------------------------------------------------------------------
Podem escollir pujar una imatge o b� un v�deo amb els botons de dalt a la dreta. Es pot pujar jpg o gif en l'apartat Image i v�deos en mp4 en l'apartat v�deo.
Es mostra una previsualitzaci� abans de pujar el fitxer i es pot afegir una descripci� del post. Per a escollir el fitxer podem pr�mer el bot� Browse o b� 
arrastrar el fitxer sobre d'aquest bot�. Una vegada es puja un post, surt un alert i tornem a home. Aquest post ser� visible en el perfil de l'usuari.

PERFIL:
-----------------------------------------------------------------------------------------------
Vista del perfil de l'usuari amb el que ens hem loguejat. Es poden veure tots els posts de l'usuari, podent fer-hi clic per veure'n el detall. A la cap�alera
es pot observar la imatge de perfil, el nom d'usuari, el nombre de publicacions, numero de seguidors i de seguits. Si fem clic a sobre dels numeros de seguidores 
i de seguits s'ens mostra una llista d'aquests, i des d'aqui podem accedir als seus perfils amb els seus posts i propietats. 
Amb el bot� editar perfil s'accedeix a la edici� del perfil i canvi de password.

CERCA:
-----------------------------------------------------------------------------------------------
Es poden buscar altres usuaris (menys el propi). El buscador retornara tots aquella usuaris que continguin la cadena introduida, pr�mer la lupa de la part drena i 
es mostraran els resultats. Podem fer clic a sobre dels usuaris per a veure'n el perfil. Si fos un usuari que no seguim, podem fer follow, o b� deixar-lo de seguir fent clic al bot� "Following".

LOGOFF:
-----------------------------------------------------------------------------------------------
Permet sortir de l'aplicaci� i entrar amb un altre usuari. Ara b�, se segueix mostrant les icones de la barra de navegaci� perqu� no es borra la sessi�.


El power-point presentaci� Instaphoto pot servir de guia per a la interficie.

