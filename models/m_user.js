/**
 * New node file
 */
module.exports = function(sequelize, DataTypes) {
    var User = sequelize.define('User', {
        name : DataTypes.STRING(255),
        lastName : DataTypes.STRING(255),
        email : DataTypes.STRING(255),
        username : {
            type: DataTypes.STRING(45),
            unique: true,
            allowNull: false
        },
        password : DataTypes.STRING(100),
        imatgePerfil : DataTypes.STRING(255),
        bio :  DataTypes.STRING(100),
        website : DataTypes.STRING(100),
        gender : DataTypes.STRING(100),
    }, {
        classMethods : {
            associate : function(models) {
                User.hasMany(models.Post);
                User.hasMany(models.Comment);
                User.hasMany(models.Like);
                User.belongsToMany(models.User, {as: 'Followers', through: 'Followers', foreignKey: 'follower_id'});
                User.belongsToMany(models.User, {as: 'Followings', through: 'Followers', foreignKey: 'following_id'});
            }
        }
    });

    return User;
};
