/**
* New node file
*/

module.exports = function(sequelize, DataTypes) {

	var Post = sequelize.define('Post', {
		postId : DataTypes.STRING(255),
		caption : DataTypes.STRING(255),
		url : DataTypes.STRING(255),
		type : DataTypes.STRING(255)
	}, {
		classMethods : {
			associate : function(models) {
				Post.belongsTo(models.User);
				Post.hasMany(models.Comment);
				Post.hasMany(models.Like);
			}
		}
	});

	return Post;
};
