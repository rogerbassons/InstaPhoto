/**
 * Created by moises on 29/03/2017.
 */
module.exports = function(sequelize, DataTypes) {
    var Comment = sequelize.define('Comment', {
        text : DataTypes.STRING(1024)
    }, {
        classMethods : {
            associate : function(models) {
                Comment.belongsTo(models.User);
                Comment.belongsTo(models.Post);
            }
        }
    });

    return Comment;
};
