/**
 * Created by moises on 29/03/2017.
 */
module.exports = function(sequelize, DataTypes) {
    var Like = sequelize.define('Like', {}, {
        classMethods : {
            associate : function(models) {
                Like.belongsTo(models.User);
                Like.belongsTo(models.Post);
            }
        }
    });

    return Like;
};