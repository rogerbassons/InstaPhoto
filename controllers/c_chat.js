/**
 * Created by moises on 1/6/17.
 */

var http = require('http')
var console = require('console');
var io = require('socket.io')
var chat = {}



chat.init = function(server, app) {

    var io = require('socket.io').listen(server);
    var dao = require('../dao')(app);
    var users = [];

    console.log("Socket server listening...")

    io.on('connection', function (socket) {
        var user={};
        dao.User.getByUsername(socket.handshake.query['username'])
            .then(function(usuari){
                user = {
                    username: usuari.username,
                    imatgePerfil: usuari.imatgePerfil,
                    status: 'online',
                    socketId: socket.id
                };
                acceptUser(socket, user);
            })




        // listen message event
        socket.on('message', function (data) {
            if (data.receiver == 'everyone') {
                socket.broadcast.emit('message', data);
            } else {
                var socketId = getSocketId(data.receiver);
                socket.broadcast.to(socketId).emit('message', data); // receiver
            }
        });

        // listen disconnect event
        socket.on('disconnect', function () {

            console.log(user.username + ' disconnected.');
            sendSysteemMessage(socket, user.username + ' disconnected.');

            // remove user from users array
            users.splice(users.indexOf(user), 1);

            console.log(users);
            io.emit('users', users);
        });

        socket.on('getusers', function () {
            io.emit('users', users);
        });

    });

    function acceptUser(socket, user) {

        var userFound = false;
        if (users) {
            for (var i = 0; i < users.length; i++) {
                if (users[i]['username'] == user.username) {
                    users[i]['status'] = 'online';
                    userFound = true;
                }
            }
        }

        if (!userFound) {
            users.push(user)
            io.to(socket.id).emit('user_validation', {
                success: true
            });

            // send systeem message to clients
            sendSysteemMessage(socket, user.username + ' joined chat.');

            // send users to all clients
            io.emit('users', users);
            console.log(user.username + ' joined chat..');
            console.log("users: ", users);

        } else {
            io.to(socket.id).emit('user_validation', {
                success: false
            });

            socket.disconnect();
        }
    }

    function sendSysteemMessage(socket, message) {

        var date = new Date();
        var systemMessage = {
            username: 'system',
            message: message,
            type: 'info',
            date: date,
            time: pad(date.getHours()) + ':' + pad(date.getMinutes())
        };
        socket.broadcast.emit('message', systemMessage);
    }

    function getSocketId(username) {
        for (var i = 0; i < users.length; i++) {
            if (users[i]['username'] == username) {
                return users[i]['socketId'];
            }
        }
    }

    function pad(value) {
        return value.toString().length > 1 ? value : '0' + value;
    }
}

module.exports = chat
