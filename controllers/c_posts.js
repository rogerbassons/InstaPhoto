module.exports = function (app) {

  var P = app.Promise;
  var db = app.db;

  var util = require('../util');
  var dao = require('../dao')(app);
  var multiparty = require('multiparty');
  var fs = require('fs');

  return {
    create: function (req, res) {
      var form = new multiparty.Form();
      var hash = require('object-hash');
      var d = new Date();
      var target_path = '';
      var description = '';
      var type = '';

      form.on('field', function(name, value) {
        if (name === 'description') {
          description = value;
        }
        else if (name === 'type') {
          type = value;
        }
      });

      form.on('file', function (name, file) {
        var tmp_path = file.path;
        var time = d.getTime()
        var id = hash(time + file.originalFilename)
        target_path = './uploads/' + id + "." + file.originalFilename.split('.').pop();
        fs.renameSync(tmp_path, target_path, function (err) {
          if (err) console.error(err.stack);
        });
        db.sequelize.transaction(function (t) {
          return dao.User.getByUsername(req.session.username, t)
            .then(function (user) {
              if (!user) util.sendError(400, util.Error.ERR_ENTITY_NOT_FOUND, "User from token does not exist");
              else return dao.Post.create(id, description, type, target_path, user, t);
            })
        })
          .then(util.jsonResponse.bind(util, res))
          .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
          .done();
      });

      form.parse(req);
    },

    getById: function (req, res) {
      if (!req.params.id) util.stdErr500(res, "Missing parameter 'id'");
      else
        db.Post.find({where: {id: req.params.id, include: [Client, Shop]}})
          .success(util.stdSeqSuccess.bindLeft(res), util.stdSeqError.bindLeft(res))
          .done();
    },

    getPostsSelf: function (req, res) {
	    dao.Post.getUserPostsByUsername(req.session.username, {})
                .then(util.jsonResponse.bind(util, res))
                .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
                .done();
    },
    getUserFeed: function (req, res) {
	   dao.Post.getMainFeed(req.session.username, {})
	    .then(util.jsonResponse.bind(util, res))
	    .catch(util.sendError.bind(util, res, 400, util.Error_BAD_REQUEST))
	    .done()
    },

    getPosts: function (req, res) {
      //dao.Post.getUserPostsById(req.params.userId, {})
      dao.Post.getUserPostsByUsername(req.params.userId, {})
        .then(util.jsonResponse.bind(util, res))
        .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
        .done();
    },

    getPostDetail: function (req, res) {
	    dao.Post.getByPostIdCommentsLikes(req.params.postId)
                .then(util.jsonResponse.bind(util, res))
                .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
                .done();
    }
  }
};
