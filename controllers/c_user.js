/**
 * New node file
 */


/*
 * GET users listing.
 */

module.exports = function (app) {

  var db = app.db;
  var secret = app.secret;
  var P = app.Promise;

  var util = require('../util');
  var dao = require('../dao')(app);
  var bcrypt = require('bcrypt-nodejs');
  var fs = require('fs');
  var multiparty = require('multiparty');

  return {
    login: function (req, res) {
      util.checkParams(req.body, ['username', 'password']);

      dao.User.checkPassword(req.body.username, req.body.password)
        .then(function (user) {
          req.session.username = user.username
          util.jsonResponse(res, {username: user.username});
        })
        .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
        .done();
    },

      create: function (req, res) {
          var form = new multiparty.Form();
          var hash = require('object-hash');
          var d = new Date();
          var target_path = null;
          var data = '';

          form.on('field', function(name, value){
              if (name === 'description') {
                  data = JSON.parse( value );
              }
          });

          form.on('file', function (name, file) {
                  var tmp_path = file.path;
                  var time = d.getTime();
                  var id = hash(time + file.originalFilename)
                  target_path = './uploads/' + id + "." + file.originalFilename.split('.').pop();

                  fs.renameSync(tmp_path, target_path, function (err) {
                      if (err) console.error(err.stack);
                  });
          });

          form.on('close', function (){
              if (target_path){
                  data.imatgePerfil=target_path;
              }
              else{
                  data.imatgePerfil="./uploads/img-default.png"
              }

              util.checkParams(data, ['name', 'lastName', 'email', 'username','imatgePerfil', 'bio', 'website', 'gender', 'password']);

              var attribs = {
                  name: data.name,
                  lastName: data.lastName,
                  email: data.email,
                  username: data.username,
                  bio: data.bio,
                  website: data.website,
                  gender: data.gender,
                  imatgePerfil: data.imatgePerfil,
                  password: bcrypt.hashSync(data.password)
              }

              db.sequelize.transaction(function (t) {
                  return Promise.all([
                          dao.User.getByEmail(req.body.email, t),
                          dao.User.getByUsername(req.body.username, t)
                      ]).then(([s1, s2]) => {
                          if (!s1 && !s2) {
                      return dao.User.create(attribs, t);
                  } else if (s1) {
                      util.throwError(400, util.Error.ERR_ENTITY_NOT_FOUND, "Already exist a User with email = " + data.email);
                  } else {
                      util.throwError(400, util.Error.ERR_ENTITY_EXISTS, "Already exist a User with username = " + data.username);
                  }
              })
              }).then(util.jsonResponse.bind(util, res))
                  .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
                  .done();
          })
          form.parse(req);
      },

    getUserSelf: function (req, res) {
      if (!req.session.username) util.sendError(res, 400, util.Error.ERR_AUTHENTICATION)
      dao.User.getByUsername(req.session.username)
          .then(util.jsonResponse.bind(util, res))
          .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
          .done();
    },

    updateUser : function (req, res) {
      dao.User.update(req.body)
          .then(util.jsonResponse.bind(util, res))
          .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
          .done();
    },

    updateImage: function (req, res) {
        if (!req.session.username) util.sendError(res, 400, util.Error.ERR_AUTHENTICATION)
        var form = new multiparty.Form();
        var hash = require('object-hash');
        var d = new Date();
        var target_path = '';
        var username = '';

        form.on('field', function(name, value) {
            if (name === 'description') {
                username = value;
            }
        });

        form.on('file', function (name, file) {
            var tmp_path = file.path;
            var time = d.getTime();
            var id = hash(time + file.originalFilename)
            target_path = './uploads/' + id + "." + file.originalFilename.split('.').pop();

            fs.renameSync(tmp_path, target_path, function (err) {
                if (err) console.error(err.stack);
            });
        });

        form.on('close', function(){
            dao.User.getByUsername(username)
                .then(function (user) {
                    if (!user) util.sendError(400, util.Error.ERR_ENTITY_NOT_FOUND, "User from token does not exist");
                    else{
                        user.set("imatgePerfil", target_path);
                        user.save()
                            .then(util.jsonResponse.bind(util, res))
                            .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_IMAGE))
                            .done();
                    }
                })
        });
        form.parse(req);
    },

    searchUsers: function(req, res){
        if (!req.session.username) util.sendError(res, 400, util.Error.ERR_AUTHENTICATION);
        dao.User.getUsersByName(req.query.name, req.session.username)
            .then(util.jsonResponse.bind(util, res))
            .catch(util.sendError.bind(util, res, 400, util.Error.ERR_DB))
            .done();
    },

    getFollowersSelf: function (req, res) {
        dao.User.getFollowersByUsername(req.session.username, {})
        .then(util.jsonResponse.bind(util, res))
        .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
        .done();
    },

    getFollowers: function (req, res) {
      //dao.User.getFollowersById(req.params.userId, {})
      dao.User.getFollowersByUsername(req.params.userId, {})
        .then(util.jsonResponse.bind(util, res))
        .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
        .done();
    },

    getFollowingsSelf: function (req, res) {
      dao.User.getFollowingsByUsername(req.session.username, {})
        .then(util.jsonResponse.bind(util, res))
        .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
        .done();
    },

    getFollowings: function (req, res) {
      dao.User.getFollowingsByUsername(req.params.userId, {})
        .then(util.jsonResponse.bind(util, res))
        .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
        .done();
    },

    getUserDetail: function (req, res) {
      //dao.User.getUserById(req.params.userId)
      dao.User.getUserByUsername(req.params.userId)
        .then(util.jsonResponse.bind(util, res))
        .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
        .done();
    },

    followUser: function (req, res) {
      db.sequelize.transaction(function (t) {
        return dao.User.getByUsername(req.session.username, t)
          .then(function (user) {
            if (!user) util.sendError(400, util.Error.ERR_ENTITY_NOT_FOUND, "User from token does not exist");
            else return dao.User.followUser(user, req.params.userId);
          })
      })
      .then(util.jsonResponse.bind(util, res))
      .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
      .done();
    },

    unfollowUser: function (req, res) {
      db.sequelize.transaction(function (t) {
        return dao.User.getByUsername(req.session.username, t)
          .then(function (user) {
            if (!user) util.sendError(400, util.Error.ERR_ENTITY_NOT_FOUND, "User from token does not exist");
            else return dao.User.unfollowUser(user, req.params.userId);
          })
      })
      .then(util.jsonResponse.bind(util, res))
      .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
      .done();
    },

    check: function (req, res) {
      if (req.session.username) util.jsonResponse(res, {username: req.session.username})
      else util.sendError(res, 400, util.Error.ERR_AUTHENTICATION, 'User has not signed up')
    }
  }
}
