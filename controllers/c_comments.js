module.exports = function (app) {

  var P = app.Promise;
  var db = app.db;

  var util = require('../util');
  var dao = require('../dao')(app);

  return {
    create: function (req, res) {
      util.checkParams(req.body, ['text'])
      if (!req.params.postId) util.stdErr500(res, "Missing parameter 'id'")

      return db.sequelize.transaction(function (t) {
        return dao.User.getByUsername(req.session.username, t)
          .then(function (user) {
            if (!user) util.sendError(400, util.Error.ERR_ENTITY_NOT_FOUND, "User from token does not exist");
            else {
              return dao.Post.getByPostId(req.params.postId, t)
              .then (function (post) {
                if (!post) util.sendError(400, util.Error.ERR_ENTITY_NOT_FOUND, "Post does not exist")
                else return dao.Comment.create(req.body, user, post, t)
              })
              .then (function (comment) {
                return dao.Post.getByPostIdCommentsLikes(req.params.postId, t)
              })
            }
          })
      })
      .then(util.jsonResponse.bind(util, res))
      .catch(util.sendError.bind(util, res, 400, util.Error.ERR_BAD_REQUEST))
      .done();
    }
  }
}
