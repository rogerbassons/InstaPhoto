/**
 * New node file
 */

module.exports = function (app, dao) {
  var util = require('../util');
  var db = app.db;
  var P = app.Promise;
  var Post = {};
  var Comment = {};

  Post.getById = function (id, t) {
    return db.Post.find(util.addTrans(t, {where: {id: id}}));
  };

  Post.getByPostId = function (postId, t) {
    return db.Post.find(util.addTrans(t, {where: {postId: postId}}));
  }

  Post.getByPostIdCommentsLikes = function (postId, t) {
    return db.Post.findOne(util.addTrans(t, {where: {postId: postId}, attributes: ['postId', 'caption', 'url', 'UserId', 'type'],
      include: [
        { model: db.User, attributes: ['username', 'imatgePerfil']},
        { model: db.Like, include: [
          { model: db.User, attributes: ['username', 'imatgePerfil']}
        ]},
        { model: db.Comment, attributes: ['text', 'createdAt'], include: [
          { model: db.User, attributes: ['username']}
        ]}
      ]
    }))
  };

  Post.getUserPosts = function (username, options, t) {
    var opt = options || {};
    return dao.User.getByUsername(username, t)
      .then(function (user) {
        if (!user) util.throwError(200, util.Error.ERR_ENTITY_NOT_FOUND, 'There is no User with username: ' + username);
        return user.getPosts(util.addTrans(t, opt));
      })
  };

  Post.getMainFeed = function (username, options, t) {
    var opt = options || {};
    return dao.User.getByUsername(username, t)
      .then(function (user) {
        if (!user) util.throwError(200, util.Error.ERR_ENTITY_NOT_FOUND, 'There is no User with username: ' + username);
        return user.getFollowings(util.addTrans(t, opt));
      })
      .then(function (followingUsers) {
	      var usernames = []
	      followingUsers.forEach(function (user) {
		      usernames.push({'username': user.username})
	      })

	      return db.Post.findAll({
		      include: [{
			      model: db.User,
			      where: {
				      $or: usernames
			      }
		      }]
	      })
      })
  };

  Post.getUserPostsByUsername = function (username, options, t) {
    var opt = options || {};
    return dao.User.getByUsername(username, t)
      .then(function (user) {
        if (!user) util.throwError(200, util.Error.ERR_ENTITY_NOT_FOUND, 'There is no User with username: ' + username);
        return user.getPosts(util.addTrans(t, opt));
      })
  };

  Post.getUserPostsById = function (userId, options, t) {
    var opt = options || {};
    return dao.User.getUserById(userId, t)
      .then(function (user) {
        if (!user) util.throwError(200, util.Error.ERR_ENTITY_NOT_FOUND, 'There is no User with username: ' + username);
        return user.getPosts(util.addTrans(t, opt));
      })
  };

  Post.create = function (postId, description, type, url, user, t) {
    return db.Post.create({postId: postId, caption: description, type: type, url: url})
      .then(function (post) {
        return post.setUser(user, util.addTrans(t, {}))
      });
  };

  return Post;
};
