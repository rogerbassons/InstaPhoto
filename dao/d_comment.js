/**
* New node file
*/

module.exports = function (app, dao) {
  var util = require('../util');
  var db = app.db;
  var P = app.Promise;
  var Comment = {};

  Comment.create = function (comment_data, user, post, t) {
    return db.Comment.create(comment_data, util.addTrans(t, {}))
    .then(function(comment) {
      return comment.setUser(user, util.addTrans(t, {}))
    })
    .then(function(comment) {
      return comment.setPost(post, util.addTrans(t, {}))
    })
  }

  return Comment;
}
