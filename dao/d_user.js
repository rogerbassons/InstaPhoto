/**
 * New node file
 */


module.exports = function (app) {
  var db = app.db;
  var User = {};

  var util = require('../util');
  var bcrypt = require('bcrypt-nodejs');

  User.checkPassword = function (username, password, t) {
    return User.getByUsername(username, t)
      .then(function (user) {
        if (user) {
          if (bcrypt.compareSync(password, user.password)) {
            return user;
          } else {
            util.throwError(400, util.Error.ERR_AUTHENTICATION, "Invalid password");
          }
        } else {
          util.throwError(400, util.Error.ERR_ENTITY_NOT_FOUND, "There is no User with username: " + username);
        }
      });
  };

  User.getByUsername = function (username, t) {
    console.log("Select a la base de dades!");
    return db.User.find(util.addTrans(t, {where: {username: username}}));
  };

  User.getByEmail = function (email, t) {
    return db.User.find(util.addTrans(t, {where: {email: email}}));
  };

  User.create = function (user_data, t) {
    return db.User.create(user_data, util.addTrans(t, {}));
  };

  User.update = function (user_data, t) {
    return db.User.update(
        user_data,
        { where: { id: user_data.id }})
  };

  User.getFollowersByUsername = function (username, options, t) {
    var opt = options || {};
    return User.getByUsername(username, t)
      .then(function (user) {
        if (user) {
          return user.getFollowers(util.addTrans(t, opt));
        } else {
          util.throwError(400, util.Error.ERR_ENTITY_NOT_FOUND, 'There is no User with username: ' + username);
        }
      });
  };

  User.getFollowersById = function (userId, options, t) {
    var opt = options || {};
    return User.getUserById(userId, t)
      .then(function (user) {
        if (user) {
          return user.getFollowers(util.addTrans(t, opt));
        } else {
          util.throwError(400, util.Error.ERR_ENTITY_NOT_FOUND, 'There is no User with user id: ' + userId);
        }
      });
  };

  User.getFollowingsByUsername = function (username, options, t) {
    var opt = options || {};
    return User.getByUsername(username, t)
      .then(function (user) {
        if (user) {
          return user.getFollowings(util.addTrans(t, opt));
        } else {
          util.throwError(400, util.Error.ERR_ENTITY_NOT_FOUND, 'There is no User with username: ' + username);
        }
      });
  };

  User.getFollowingsById = function (userId, options, t) {
    var opt = options || {};
    return User.getUserById(userId, t)
      .then(function (user) {
        if (user) {
          return user.getFollowings(util.addTrans(t, opt));
        } else {
          util.throwError(400, util.Error.ERR_ENTITY_NOT_FOUND, 'There is no User with user id: ' + userId);
        }
      });
  };

  User.followUser = function (session_user, username_follow) {
    return User.getUserByUsername(username_follow, "")
      .then(function(user) {
        if (user) {
          return session_user.addFollowing(user);
        } else {
          util.throwError(400, util.Error.ERR_ENTITY_NOT_FOUND, 'There is no User with username: ' + username_follow);
        }
      });
  };

  User.unfollowUser = function (session_user, username_follow) {
    return User.getUserByUsername(username_follow, "")
      .then(function(user) {
        if (user) {
          return session_user.removeFollowing(user);
        } else {
          util.throwError(400, util.Error.ERR_ENTITY_NOT_FOUND, 'There is no User with username: ' + username_follow);
        }
      });
  };

  User.getUserByUsername = function (username, t) {
    return db.User.find(util.addTrans(t, {where: {username: username}}));
  };

  User.getUserById = function (userId, t) {
    return db.User.find(util.addTrans(t, {where: {id: userId}}));
  };

  User.getUsersByName = function(patternUsername, username){
    var param = "%" + patternUsername + "%";
    return db.sequelize.query("SELECT username, imatgePerfil FROM users WHERE username LIKE :patternUsername AND username NOT LIKE :username ",
        {replacements: {patternUsername : param, username: username}, type: db.sequelize.QueryTypes.SELECT })
  }

  return User;
}
