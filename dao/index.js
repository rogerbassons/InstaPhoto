/**
 * New node file
 */

module.exports = function (app) {
  var dao = {};

  dao.User = require('./d_user')(app, dao);
  dao.Post = require('./d_post')(app, dao);
  dao.Comment = require('./d_comment')(app, dao);
  dao.Like = require('./d_like')(app, dao);

  return dao;
}
