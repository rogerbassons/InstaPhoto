/**
* New node file
*/

module.exports = function (app, dao) {
  var util = require('../util');
  var db = app.db;
  var P = app.Promise;
  var Like = {};

  Like.create = function (user, post, t) {
    return db.Like.create(util.addTrans(t, {}))
    .then(function(like) {
      return like.setUser(user, util.addTrans(t, {}))
    })
    .then(function(like) {
      return like.setPost(post, util.addTrans(t, {}))
    })
  }

  return Like;
}
