# nodebb

This is a simple node.js application showing how to combine:

- Express.js 4
- Sequelize 3
- Webpack 2
- Backbone
- Bootstrap 3

To run the application:

- Execute `npm install` at the root of the project
- Execute `npm install` inside the `public` folder
- Execute `npm run webpack` inside the `public` folder
- Execute `node server.js` at the root of the project
- Load `http://localhost:8080`

Remember that everytime that you change any javascript file inside the `public` folder, you have to run `npm run webpack` to regenerate the file `main.min.js` and see the changes

