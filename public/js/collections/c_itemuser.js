/**
 * Created by moises on 31/5/17.
 */
var ItemUserModel = require('../models/m_itemuser');

var ItemUserCollection = Backbone.Collection.extend({
    model: ItemUserModel,
    url: "/api/users/search"
});

module.exports = ItemUserCollection;
