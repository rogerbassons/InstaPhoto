/**
 * @author: Robert Garcia Ventura
 * @date: 20/05/2017
 */

var UserModel = require('../models/m_user');

var FollowersCollection = Backbone.Collection.extend({
  model: UserModel,
  url: "/api/users/self/followers",

  initialize: function(params) {
    this.model = UserModel;
    if(params.id == null){
      this.url = "/api/users/self/followers";
    }else{
      this.url = "/api/users/" + params.id + "/followers";
    }
  }
});

module.exports = FollowersCollection;
