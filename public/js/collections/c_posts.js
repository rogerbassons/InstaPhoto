var PostModel = require('../models/m_post');

var PostCollection = Backbone.Collection.extend({
  model: PostModel,
  url: "/api/users/self/posts",

  initialize: function(params) {
    this.model = PostModel;
    if(params.id == null){
      this.url = "/api/users/self/posts";
    }else{
      this.url = "/api/users/" + params.id + "/posts";
    }
  }
});

module.exports = PostCollection;
