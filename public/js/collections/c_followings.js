/**
 * @author: Robert Garcia Ventura
 * @date: 20/05/2017
 */

var UserModel = require('../models/m_user');

var FollowingsCollection = Backbone.Collection.extend({
  model: UserModel,
  url: "/api/users/self/followings",

  initialize: function(params) {
    this.model = UserModel;
    if(params.id == null){
      this.url = "/api/users/self/followings";
    }else{
      this.url = "/api/users/" + params.id + "/followings";
    }
  }
});

module.exports = FollowingsCollection;
