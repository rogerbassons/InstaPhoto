var localStorage = require('../localStorage')

var Presenters = {}

var Ui = require('./ui')
var Router = require('./router')
var Login = require('./login')
var Post = require('./post')
var Api = require('./api')
var User = require('./user')
var EditProfile = require('./editProfile')

Presenters.init = function() {
  Ui.init()
  Login.init()
  Post.init()
  EditProfile.init()
  Api.init()
  User.init()
  Api.checkActiveSession()
  .then(function(user) {
    localStorage.setItem('user', user)
    Router.init()
  })
  .catch(function() {
    localStorage.removeItem('user')
    Router.init()
  })
  .done()
}

module.exports = Presenters
