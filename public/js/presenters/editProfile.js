/**
 * Created by Moises on 18/05/2017.
 */
var EventBus = require('../eventBus')


var EditProfile = {}

EditProfile.init = function () {
    EventBus.on('view:editProfile:uploadImage', EventBus.trigger.bind(EventBus, 'api:uploadImageProfile'))
    EventBus.on('view:editProfile:changePass', EventBus.trigger.bind(EventBus, 'ui:switch:editProfile'))
    EventBus.on('view:editProfile:succesful', EventBus.trigger.bind(EventBus, 'ui:switch:profile'))
}

module.exports = EditProfile