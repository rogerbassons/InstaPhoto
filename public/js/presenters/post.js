var EventBus = require('../eventBus')

var Post = {}

Post.init = function () {
  EventBus.on('view:post:get', EventBus.trigger.bind(EventBus, 'ui:switch:post'));
  EventBus.on('view:post:addComment', EventBus.trigger.bind(EventBus, 'ui:switch:addComment'));
  EventBus.on('view:post:upload', EventBus.trigger.bind(EventBus, 'api:post:upload'));
  EventBus.on('view:post:like', EventBus.trigger.bind(EventBus, 'ui:switch:likePost'));
  EventBus.on('view:post:unLike', EventBus.trigger.bind(EventBus, 'ui:switch:unLikePost'));
}

module.exports = Post
