var localStorage = require('../localStorage')
var EventBus = require('../eventBus')

var Api = {};

Api.login = function (data) {
  return $.ajax({
    url: '/api/users/login',
    dataType: 'json',
    type: 'post',
    contentType: 'application/json',
    data: JSON.stringify(data),
    processData: false
  })
};

Api.signup = function (data) {
    return $.ajax({
        url: '/api/users',
        type: 'post',
        contentType: false,
        data: data,
        processData: false
    });
};

Api.upload = function (data) {
  return $.ajax({
    url: '/api/media',
    type: 'post',
    contentType: false,
    processData: false,
    data: data
  });
};

Api.uploadImageProfile = function (data) {
  return $.ajax({
      url: '/api/users/self/image',
      type: 'post',
      contentType: false,
      processData: false,
      data: data
  });
}

Api.checkActiveSession = function() {
  return $.getJSON('/api/users/self')
}

Api.init = function () {

// Login

  EventBus.on('api:login', function (username, password) {
    Api.login({username: username, password: password})
      .then(EventBus.trigger.bind(EventBus, 'api:login:successful'))
      .catch(EventBus.trigger.bind(EventBus, 'api:login:error'))
      .done()
  });

  EventBus.on('api:login:successful', function (user) {
    localStorage.setItem('user', user);
    EventBus.trigger('ui:showHome')
  });

  EventBus.on('api:login:error', EventBus.trigger.bind(EventBus, 'ui:showError'));

// Signup
  EventBus.on('api:signup', function (image, data) {
      var form = new FormData();
      var infoUser = JSON.stringify(data);
      console.log(data);
      console.log(infoUser);
      form.append('description', infoUser);
      form.append('file', image);
      Api.signup(form)
        .then(EventBus.trigger.bind(EventBus, 'api:signup:successful'))
        .catch(EventBus.trigger.bind(EventBus, 'api:signup:error'))
        .done()
  });


  EventBus.on('api:signup:successful', function (user) {
    EventBus.trigger('ui:showHome')
  });

  EventBus.on('api:signup:error', EventBus.trigger.bind(EventBus, 'ui:showError'));

// Upload post

  EventBus.on('api:post:upload', function(file, description, type){
      var data = new FormData();
      data.append('file', file);
      data.append('description', description);
      data.append('type', type);
      Api.upload(data)
        .then(alert("Post created"))
        .then(EventBus.trigger('ui:switch:home'))
        .catch(EventBus.trigger.bind(EventBus, 'ui:showError'))
        .done();
  });

  EventBus.on('api:uploadImageProfile', function( image, username ){
    var form = new FormData();
    form.append('file', image);
    form.append('description', username)
    Api.uploadImageProfile(form)
        .then(EventBus.trigger.bind(EventBus, 'api:uploadImageProfile:successful'))
        .catch(EventBus.trigger.bind(EventBus, 'ui:showError'))
        .done();
  });

    EventBus.on('api:uploadImageProfile:successful', function (user) {
        EventBus.trigger('ui:switch:profile', user.username);
    });
};

module.exports = Api;

