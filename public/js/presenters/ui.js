var EventBus = require('../eventBus')
var localStorage = require('../localStorage')
var CollectionPost = require("../collections/c_posts")
var Post = require("../models/m_post")
var Comment = require("../models/m_comment")
var Like = require("../models/m_like")
var UserLogin = require("../views/user/v_login")
var UserSignup = require("../views/user/v_signup")
var HeaderView = require("../views/header")
var PostsView = require("../views/post/vl_posts")
var UserProfile = require("../views/profile/v_profile")
var PostDetailView = require("../views/post/vl_postDetail")
var PostUpload = require("../views/post/v_postUpload")
var EditProfile = require("../views/profile/v_editProfile")
//var Profile = require("../models/m_profile")
var changePassword = require("../views/profile/v_changePass")
var Followers = require("../views/profile/v_followers")
var CollectionFollowers = require("../collections/c_followers")
var Following = require("../views/profile/v_following")
var Search = require("../views/search/v_search")

var Chat = require("../views/chat/v_chat")
var CollectionFollowings = require("../collections/c_followings")
var User = require("../models/m_user")
var Profile = require("../models/m_user")

var Ui = {}

var feedList = new CollectionPost({})
var postList = new CollectionPost({id: "jo"})
var followersList = new CollectionFollowers({id: "jo"})
var followingList = new CollectionFollowings({id: "jo"})

var $content = $('#content')

var lastView = null;
var lastViewHeader = null;
var chatView=null;


Ui.switchContent = function (widget) {

  var args = Array.prototype.slice.call(arguments)
  args.shift()
  if (lastView !== null){
      if (lastView != chatView){
          lastView.undelegateEvents()
      }

  }
  switch (widget) {
    case 'login': {
      $("#bigIcon").show();
      lastView = new UserLogin({el: $content, eventBus: EventBus}).render()
      break
    }
    case 'signup': {
      $("#bigIcon").hide();
      lastView = new UserSignup({el: $content, eventBus: EventBus}).render()
      break
    }
    case 'editProfile': {
      $("#bigIcon").hide();
      $("#mainTitle").hide();
      new Profile({id: args[0]}).fetch({
        success: function(profile) {
          lastView = new EditProfile({el: $content, eventBus: EventBus, model: profile}).render()
        }
      })
      break
    }
    case 'changePass': {
        $("#bigIcon").hide();
        $("#mainTitle").hide();
        new Profile({id: args[0]}).fetch({
            success: function(profile) {
                lastView = new changePassword({el: $content, eventBus: EventBus, model: profile}).render()
            }
        })
        break
    }
    case 'feed': {
      $("#bigIcon").hide();
      if (localStorage.hasItem('user')) {
        feedList.url = "/api/users/self/feed"
        feedList.fetch({
          success: function () {
            lastView = new PostsView({el: $content, eventBus: EventBus, collection: feedList}).render()
          },
          error: Ui.error
        });
      }
      break
    }
    case 'post': {
      $("#bigIcon").hide();
      $("#mainTitle").show();
      if (localStorage.hasItem('user')) {
        post = new Post({id: args[0]})
        post.fetch({
          success: function () {
            var user = localStorage.getItem('user').username;
            lastView = new PostDetailView({el: $content, eventBus: EventBus, model: post, user: user}).render()
          },
          error: Ui.error
        });
      }
      break
    }
    case 'addComment': {
      $("#bigIcon").hide();
      $("#mainTitle").show();
      if (localStorage.hasItem('user')) {
        comment = new Comment({text: args[1]})
        comment.url = "/api/posts/" + args[0] + "/comments"
        comment.save(null, {
          success: function (res) {
            var user = localStorage.getItem('user').username;
            lastView = new PostDetailView({el: $content, eventBus: EventBus, model: new Post(res.attributes), user: user}).render()
          },
          error: Ui.error
        });
      }
      break
    }
    case 'likePost': {
      $("#bigIcon").hide();
      $("#mainTitle").show();
      if (localStorage.hasItem('user')) {
        like = new Like()
        like.url = "/api/posts/" + args[0] + "/likes"
        like.save(null, {
          success: function (res) {
            var user = localStorage.getItem('user').username;
            lastView = new PostDetailView({el: $content, eventBus: EventBus, model: new Post(res.attributes), user: user}).render()
          },
          error: Ui.error
        });
      }
      break
    }
    case 'unLikePost': {
      $("#bigIcon").hide();
      $("#mainTitle").show();
      if (localStorage.hasItem('user')) {
        like = new Like()
        like.url = "/api/posts/" + args[0] + "/unlike"
        like.save(null, {
          success: function (res) {
            var user = localStorage.getItem('user').username;
            lastView = new PostDetailView({el: $content, eventBus: EventBus, model: new Post(res.attributes), user: user}).render()
          },
          error: Ui.error
        });
      }
      break
    }

    case 'profile': {
      $("#bigIcon").hide();
      $("#mainTitle").hide();

      if (localStorage.hasItem('user')) {
        new Profile({id: args[0]}).fetch({
          success: function (user) {
            var postList = new CollectionFollowers({eventBus: EventBus, id: args[0]});
            postList.fetch({
              success: function() {
                lastView = new UserProfile({el: $content, eventBus: EventBus, model: user, collection: postList}).render();
              },
              error: Ui.error
            });
          },
          error: Ui.error
        });
      }
      break
    }
    case 'logout': {
      $("#bigIcon").show();
      lastView = new UserLogin({el: $content, eventBus: EventBus}).render()
      break
    }
    case 'add': {
      $("#bigIcon").hide();
      lastView = new PostUpload({el: $content, eventBus: EventBus}).render()
      break
    }
    case 'search':{
        $("#bigIcon").hide();
        lastView = new Search({el: $content, eventBus: EventBus}).render()
        break
    }
    case 'followers': {
      $("#bigIcon").hide();
      $("#mainTitle").hide();
      followersList = new CollectionFollowers({id: args[0]});
      followersList.fetch({
        success: function () {
          lastView = new Followers({el: $content, eventBus: EventBus, model: profile, collection: followersList}).render();
        },
        error: Ui.error
      });
      break;
    }
    case 'chat' : {
        $("#bigIcon").hide();
        $("#mainTitle").hide();

        if (!chatView){
            var user = localStorage.getItem('user').username;
            chatView = new Chat({el: $content, eventBus: EventBus, username: user}).render();
            lastView = chatView;
        }
        lastView=chatView;
        lastView.render();
        break
    }
    case 'following': {
      $("#bigIcon").hide();
      $("#mainTitle").hide();
      followingList = new CollectionFollowings({id: args[0]});
      followingList.fetch({
        success: function () {
          lastView = new Following({el: $content, eventBus: EventBus, model: profile, collection: followingList }).render();
        },
        error: Ui.error
      });
      break;
    }
    case 'user': {
      $("#bigIcon").hide();
      $("#mainTitle").hide();
      if (localStorage.hasItem('user')) {
        user = new User({id: args[0]})
        user.fetch({
          success: function () {
            postList = new CollectionPost({id: args[0]});
            followingList = new CollectionFollowings({id: args[0]});
            followersList = new CollectionFollowers({id: args[0]});
            postList.fetch({
              success: function () {
                followingList.fetch({
                  success: function () {
                    followersList.fetch({
                      success: function () {
                        lastView = new UserProfile({el: $content, eventBus: EventBus, model: user, collection_postList: postList, collection_followingList: followingList, collection_followersList: followersList}).render();
                      },
                      error: Ui.error
                    });
                  },
                  error: Ui.error
                });
              },
              error: Ui.error
            });
          },
          error: Ui.error
        });
      }
    }
  }
}

Ui.init = function () {
   // headerView.setUserData(localStorage.getItem('user'))
    //Ui.showHome();
}

Ui.showHome = function () {
  if (lastViewHeader !== null){
    lastViewHeader.undelegateEvents()
  }
  lastViewHeader = new HeaderView({el: '#header', eventBus: EventBus, user: localStorage.getItem('user')}).render()
  if (localStorage.hasItem('user')) {
    Ui.switchContent('feed')
  } else {
    Ui.switchContent('login')
  }
}

Ui.showSignup = function () {
  Ui.switchContent('signup')
}

// This always receive a JSON object with a standard API error
Ui.error = function (err) {
  if (err.message)
    alert("Error: " + err.message)
  else if (err.responseJSON) {
    if (err.responseJSON.message)
      alert("Error: " + err.responseJSON.message)
    else if (err.responseJSON.error)
      alert("Error: " + err.responseJSON.error.message)
  }
}

Ui.showProfile = function () {
    Ui.switchContent('profile');
}

Ui.showLogout = function () {

    Ui.switchContent('logout');
}

Ui.showAdd = function () {

  Ui.switchContent('add');
}

Ui.showEditProfile = function () {
    Ui.switchContent('editProfile');
}

Ui.showChangePass = function () {
  Ui.switchContent('changePass')
}

Ui.showFollowers = function () {
    Ui.switchContent('followers')
}

Ui.showFollowing = function () {
    Ui.switchContent('following')
}

Ui.showSearch = function () {
    Ui.switchContent('search')
}

Ui.showLogin = function () {
    Ui.switchContent('login');
}

Ui.showXat = function (){
    Ui.switchContent('chat');
}

Ui.closeXat = function (){
    chatView = null;
    Ui.showHome();
}

EventBus.on('ui:showHome', Ui.showHome);
EventBus.on('ui:showError', Ui.error);
EventBus.on('ui:switch:signup', Ui.showSignup);
EventBus.on('ui:switch:posts', Ui.switchContent.bind(null, 'posts'));
EventBus.on('ui:switch:post', Ui.switchContent.bind(null, 'post'));
EventBus.on('ui:switch:likePost', Ui.switchContent.bind(null, 'likePost'));
EventBus.on('ui:switch:unLikePost', Ui.switchContent.bind(null, 'unLikePost'));
EventBus.on('ui:switch:addComment', Ui.switchContent.bind(null, 'addComment'));
EventBus.on('ui:switch:home', Ui.showHome);
EventBus.on('ui:switch:xats', Ui.showXats);
EventBus.on('ui:switch:activity', Ui.showActivity);
EventBus.on('ui:switch:add', Ui.showAdd);
EventBus.on('ui:switch:profile', Ui.switchContent.bind(null, 'user'));
EventBus.on('ui:switch:search', Ui.showSearch);
EventBus.on('ui:switch:logout', Ui.showLogout);
EventBus.on('ui:switch:editProfile', Ui.switchContent.bind(null, 'editProfile'));
EventBus.on('ui:switch:goChangePass', Ui.switchContent.bind(null, 'changePass'));
EventBus.on('ui:switch:followers', Ui.switchContent.bind(null, 'followers'));
EventBus.on('ui:switch:following', Ui.switchContent.bind(null, 'following'));
EventBus.on('ui:switch:user', Ui.switchContent.bind(null, 'user'));
EventBus.on('ui:switch:login', Ui.showLogin);
EventBus.on('ui:switch:xats', Ui.showXat);
EventBus.on('ui:switch:closechat', Ui.closeXat)

module.exports = Ui
