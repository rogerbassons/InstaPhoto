var EventBus = require('../eventBus')
var localStorage = require('../localStorage')

var Router = {}

Router.init = function () {
  var AppRouter = Backbone.Router.extend({
    routes: {
      // Define some URL routes
      '': 'home',
      'signup': 'signup',
      'login': 'home',
      'posts': 'showPosts',
      'home': 'showHome',
      'xats': 'showXats',
      'activity': 'showActivity',
      'add': 'showAdd',
      'profile': 'showProfile',
      'search': 'showSearch',
      'logout': 'showLogout',
      'editProfile' : 'editProfile',

      // Default
      '*actions': 'defaultAction'
    },

    home: function () {
      EventBus.trigger('ui:showHome')
    },

    signup: function () {
      EventBus.trigger('ui:switch:signup')
    },

    showPosts: function () {
      EventBus.trigger('ui:switch:posts')
    },

    showHome: function() {
      EventBus.trigger('ui:switch:home')
    },

    showXats: function() {
      EventBus.trigger('ui:switch:xats')
    },

    showActivity: function() {
      EventBus.trigger('ui:switch:activity')
    },

    showAdd: function() {
      EventBus.trigger('ui:switch:add')
    },

    showProfile: function() {
      EventBus.trigger('ui:switch:profile')
    },

    showSearch: function() {
      EventBus.trigger('ui:switch:search')
    },

    editProfile: function() {
      EventBus.trigger('ui:switch:editProfile')
    },

    showLogout: function() {
      EventBus.trigger('ui:switch:logout')
    },

    defaultAction: function() {
      console.log("hola")
    }
  })

  new AppRouter()

  Backbone.history.start()
}

module.exports = Router
