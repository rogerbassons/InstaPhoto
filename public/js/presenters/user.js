/**
 * @author: Robert Garcia Ventura
 * @date: 04/06/2017
 */

var EventBus = require('../eventBus')

var User = {}

User.init = function () {
  EventBus.on('view:user:get', EventBus.trigger.bind(EventBus, 'ui:switch:user'));
}

module.exports = User
