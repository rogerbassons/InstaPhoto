var t_signup = require("raw-loader!../../../templates/user/signup.html")
var $ = jQuery = require('jquery');
var bootstrapImgUpload = require("bootstrap-imageupload");


var UserSignup = Backbone.View.extend({

  className: 'container',

  template: _.template(t_signup),

  initialize: function(params) {
    this.eventBus = params.eventBus;
  },

  events: {
    'click  #btn-signup' : 'submit',
    'click #btn-cancel' : 'click_cancel'
  },


  submit: function () {
      if((this.$('[name=firstname]').val().length == 0) || (this.$('[name=lastname]').val().length == 0) || (this.$('[name=email]').val().length == 0) || (this.$('[name=username]').val().length == 0)
          || (this.$('[name=pass]').val().length == 0)){
        alert( "Missing fields" );
      }
      else if (($("#password1").val() == $("#password2").val()))  {
        var data = {
          name: this.$('[name=firstname]').val(),
          lastName: this.$('[name=lastname]').val(),
          email: this.$('[name=email]').val(),
          username: this.$('[name=username]').val(),
          bio: this.$('[name=bio]').val(),
          website: this.$('[name=website]').val(),
          gender: this.$('#gender').val(),
          password: this.$('[name=pass]').val()
        }
        this.eventBus.trigger('view:signup:request',this.$el.find('#inputFile').prop('files')[0],  data )

      }
      else{
          alert( "Passwords must match" );
      }
  },

  click_cancel: function() {
      this.eventBus.trigger('ui:switch:login')
  },

  render: function () {
    this.$el.html(this.template());
    var arrel = this;
    this.$('#inputFile').change(function () {
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                arrel.$('#imageProfile').attr('src', e.target.result);
            };
            reader.readAsDataURL(this.files[0]);
        }
    });
    return this
  }
})

module.exports = UserSignup