/**
 * Created by Moises on 03/06/2017.
 */
var t_userItem = require("raw-loader!../../../templates/search/t_userItem.html")

var UserItemView = Backbone.View.extend({

    initialize: function(params) {
        this.eventBus = params.eventBus;
        this.template = _.template(t_userItem);
    },
    tagName: "li",

    render: function () {
        this.$el.html(this.template({user: this.model}))
        return this
    }

});

module.exports = UserItemView;