var t_login = require("raw-loader!../../../templates/user/login.html")


var UserLogin = Backbone.View.extend({

  template: _.template(t_login),

  className: 'container',

  initialize: function(params) {
    this.eventBus = params.eventBus;
  },

  events: {
    'click .btn-login': 'submit',
    'keypress': 'submitEnter'
  },

  submit: function () {
    this.eventBus.trigger('view:login:request', this.$('#login-username').val(), this.$('#login-password').val())
  },

  submitEnter: function (e) {
    if (e.which === 13) {
      this.submit()
    }
  },

  render: function () {
    this.$el.html(this.template())
    return this
  }

})

module.exports = UserLogin