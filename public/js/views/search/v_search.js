/**
 * Created by moises on 31/5/17.
 */
var tl_search= require("raw-loader!../../../templates/search/t_search.html");
var UserItemView = require('../user/v_userItem');
var CollectionItemUser=require("../../collections/c_itemuser")

var Search = Backbone.View.extend({

    initialize: function(params) {
        this.eventBus = params.eventBus;
        this.template = _.template(tl_search);

        this.localEventBus = _.extend({}, Backbone.Events);
        this.collection = new  CollectionItemUser({})

    },

    className: 'container',

    events: {
        'click #search': 'getUsers',
        'keypress': 'submitEnter',
        'click .list-group li': 'selectUser'
    },

    selectUser: function (event) {
      this.eventBus.trigger('view:user:get', event.currentTarget.id);
    },

    getUsers: function () {
        var self = this;
        var patternUsername = this.$('#input').val();
        var llista = this.$('.list-group');

        //Reset collection and list
        this.collection.reset();
        llista.empty();

        if (patternUsername.length > 0){

            this.collection.fetch({
                data: $.param({ name: patternUsername}),
                success: function () {
                    var $userList = self.$el.find('.list-group');
                    var localEventBus = this.localEventBus
                    self.collection.each(function(user){
                        $userList.append(new UserItemView({id: user.get('username'), model: user, eventBus: localEventBus}).render().el)
                    })
                }
            });
        }


    },

    submitEnter: function (e) {
        if (e.which === 13) {
            this.getUsers();
        }
    },

    render: function () {
        this.$el.html(this.template({ users: this.collection}))
        return this
    }

});

module.exports = Search
