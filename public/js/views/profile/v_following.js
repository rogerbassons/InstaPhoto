/**
 * @author: Robert Garcia Ventura
 * @date: 07/05/2017
 */

var t_following = require("raw-loader!../../../templates/profile/t_following.html")
var UserView = require("./v_user")
var CollectionFollowings = require("../../collections/c_followings")

var Following = Backbone.View.extend({

  className: 'container',

  initialize: function(params) {
    this.eventBus = params.eventBus;
    this.template = _.template(t_following);
    this.localEventBus = _.extend({}, Backbone.Events) //EventBus local per rebre els events de "click" de les subvistes (vistes que conté aquesta vista)
    this.localEventBus.on('view:user:profile', this.showDetail.bind(this))
    console.log("Inici v_following");
  },

  events: {

  },

  showDetail: function(id) {
    console.log("User ID: " + id.toString());
    this.eventBus.trigger('view:user:get', id)
  },

  render: function () {

    //Mostrem la capçalera
    this.$el.html(this.template({User: this.model}))

    //Mostrem els followings de l'usuari
    var $followingsList = this.$el.find(".users_list");
    var localEventBus = this.localEventBus;

    this.collection.each(function(user) {
      // -- Obtenir si l'usuari 'user' està dins els seguidors de l'usuari que té la sessió --
      var perfil_usuari = "";
      var usuari_local = "";
      var followingUser = false;
      var followingList = new CollectionFollowings({id: JSON.parse(localStorage.getItem('user')).username});
      followingList.fetch({
        success: function () {
          followingList.each(function(usuari_followingList) {
            perfil_usuari = usuari_followingList.get('username');
            usuari_local = user.get('username');
            if(perfil_usuari == usuari_local && !followingUser){
              followingUser = true;
            }
          });
          if(followingUser){
            $followingsList.append(new UserView({eventBus: localEventBus, model: user, tipusRelacio: "Following"}).render().el)
          }else if(user.get('username') == JSON.parse(localStorage.getItem('user')).username){
            $followingsList.append(new UserView({eventBus: localEventBus, model: user, tipusRelacio: ""}).render().el)
          }else{
            $followingsList.append(new UserView({eventBus: localEventBus, model: user, tipusRelacio: "Follow"}).render().el)
          }
        },
        error: console.log("Error: v_user <- render()")
      });
      // -- FI Obtenir si l'usuari 'user' està dins els seguidors de l'usuari que té la sessió --
    })
    return this
  }
})

module.exports = Following