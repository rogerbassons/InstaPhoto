/**
 * @author: Robert Garcia Ventura
 * @date: 10/05/2017
 */

var t_user = require("raw-loader!../../../templates/profile/t_user.html")
var CollectionFollowings = require("../../collections/c_followings")
var FollowingModel = require("../../models/m_following")
var followingUser = false;

var UserView = Backbone.View.extend({

  initialize: function(params) {
    this.eventBus = params.eventBus;
    this.template = _.template(t_user);
    this.tipusRelacio = params.tipusRelacio;
    if(params.tipusRelacio == "Following"){
      this.followingUser = true;
    }
  },

  events: {
    'click .img_user': 'userProfile',
    'click .user_username': 'userProfile',
    'click .user_button_follow': 'followButton'
  },

  render: function () {
    this.$el.html(this.template({user: this.model, follower_button: this.tipusRelacio}));
    var boto_perfil = this.$el.find(".user_button_follow");
    if(this.followingUser){
      boto_perfil.addClass("button_following");
    }else{
      boto_perfil.addClass("button_follow");
    }
    return this
  },

  userProfile: function() {
    this.eventBus.trigger('view:user:profile', this.model.get('username'))
  },

  followButton: function () {
    var boto_perfil = this.$el.find(".user_button_follow");
    var self = this;
    if(this.followingUser){ //Unfollow user
      if(JSON.parse(localStorage.getItem('user')).username != this.model.get('username')) {
        followingModel = new FollowingModel({tipusRelacio: "Following", username: this.model.get('username')});
        followingModel.save(null, {
          success: function () {
            console.log("S'ha deixat de seguir al usuari: " + self.model.get('username'));
          }
        }).catch(function (err) {
          console.log(`Error: ${err}`)
        });
        boto_perfil.text("Follow");
        boto_perfil.addClass("button_follow");
        boto_perfil.removeClass("button_following");
        this.followingUser = false;
      }
    }else{ //Follow user
      if(JSON.parse(localStorage.getItem('user')).username != this.model.get('username')){
        followingModel = new FollowingModel({tipusRelacio: "Follow",username: this.model.get('username')});
        followingModel.save(null, {
          success: function() {
            console.log("S'ha començat a seguir al usuari: " + self.model.get('username'));
          }
        }).catch(function(err) {
          console.log(`Error: ${err}`)
        });
        boto_perfil.text("Following");
        boto_perfil.addClass("button_following");
        boto_perfil.removeClass("button_follow");
        this.followingUser = true;
      }
    }
    console.log("Usuari " + this.model.get('username') + " follow button")
  },
});

module.exports = UserView
