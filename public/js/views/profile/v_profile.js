/**
 * @author: Robert Garcia Ventura
 * @date: 05/04/2017
 */

var t_profile = require("raw-loader!../../../templates/profile/t_profile.html")
var PostItemView = require("../post/vl_postItem")
var Followers = require("./v_followers")
var Following = require("./v_following")
var CollectionFollowings = require("../../collections/c_followings")
var FollowingModel = require("../../models/m_following")
var followingUser = false;

var UserProfile = Backbone.View.extend({

    className: 'container',

    initialize: function(params) {
        this.eventBus = params.eventBus;
        this.template = _.template(t_profile);
        this.collection_postList = params.collection_postList;
        this.collection_followingList = params.collection_followingList;
        this.collection_followersList = params.collection_followersList;

        if(params.model.get('username') == JSON.parse(localStorage.getItem('user')).username){
          this.followingUser = true;
        }

        this.localEventBus = _.extend({}, Backbone.Events) //EventBus local per rebre els events de "click" de les subvistes (vistes que conté aquesta vista)
    },

    events: {
        'click .image_button_profile': 'click_photo_profile',
        'click .click_profile_button': 'click_profile_button',
        'click .user_followers': 'click_user_followers',
        'click .user_following': 'click_user_following'
    },

    click_photo_profile: function () {

    },

    click_profile_button: function () {
      var boto_perfil = this.$el.find(".click_profile_button");
      var self = this;
      switch(boto_perfil.text()) {
        case "Editar perfil":
          this.eventBus.trigger('ui:switch:editProfile', this.model.get('username'));
          break;
        case "Following":
          if(JSON.parse(localStorage.getItem('user')).username != this.model.get('username')) {
            followingModel = new FollowingModel({tipusRelacio: "Following", username: this.model.get('username')});
            followingModel.save(null, {
              success: function () {
                console.log("S'ha deixat de seguir al usuari: " + self.model.get('username'));
              }
            }).catch(function (err) {
              console.log(`Error: ${err}`)
            });
            boto_perfil.text("Follow");
            boto_perfil.addClass("button_follow");
            boto_perfil.removeClass("button_following");
            this.followingUser = false;
          }
          break;
        case "Follow":
          if(JSON.parse(localStorage.getItem('user')).username != this.model.get('username')){
            followingModel = new FollowingModel({tipusRelacio: "Follow",username: this.model.get('username')});
            followingModel.save(null, {
              success: function() {
                console.log("S'ha començat a seguir al usuari: " + self.model.get('username'));
              }
            }).catch(function(err) {
              console.log(`Error: ${err}`)
            });
            boto_perfil.text("Following");
            boto_perfil.addClass("button_following");
            boto_perfil.removeClass("button_follow");
            this.followingUser = true;
          }
          break;
        default:
          console.log("(v_profile) --> opció no existeix")
          break;
      }
    },

    click_user_followers: function () {
      //this.eventBus.trigger('ui:switch:followers', this.model.get('username'));
      new Followers({el: $('#content'), eventBus: this.eventBus, model: this.model, collection: this.collection_followersList}).render();
    },

    click_user_following: function () {
      //this.eventBus.trigger('ui:switch:following', this.model.get('username'));
      new Following({el: $('#content'), eventBus: this.eventBus, model: this.model, collection: this.collection_followingList }).render();
    },

    render: function () {
      console.log(this.model.toJSON());

      //Mostrem la capçalera del perfil
      var perfil_usuari = this.model.get('username');
      var usuari_local = JSON.parse(localStorage.getItem('user')).username;

      if(perfil_usuari == usuari_local){
        this.$el.html(this.template({profile: this.model, profile_button: "Editar perfil", num_publications: this.collection_postList.length, num_followers: this.collection_followersList.length, num_following: this.collection_followingList.length}))
        boto_perfil = this.$el.find(".click_profile_button");
        boto_perfil.addClass("button_following");

        //Mostrem les imatges de l'usuari
        var galeria_imatges = this.$el.find(".galeria_imatges_usuari")
        var eb = this.eventBus
        this.collection_postList.each(function(post) {
          var item = new PostItemView({eventBus: eb, model: post}).render().el
          galeria_imatges.append(item)
        })

      }else{
        // -- Obtenir si l'usuari 'user' està dins els seguidors de l'usuari que té la sessió --
        var self = this;
        this.followingUser = false;
        var followingList = new CollectionFollowings({id: usuari_local});
        followingList.fetch({
          success: function () {
            followingList.each(function(usuari_followingList) {
              if((perfil_usuari == usuari_followingList.get('username')) && !followingUser){
                self.followingUser = true;
              }
            });
          },
          error: console.log("Error: v_user <- render()")
        }).then(function () {
          if(self.followingUser){
            self.$el.html(self.template({profile: self.model, profile_button: "Following", num_publications: self.collection_postList.length, num_followers: self.collection_followersList.length, num_following: self.collection_followingList.length}))
            boto_perfil = self.$el.find(".click_profile_button");
            boto_perfil.addClass("button_following");
          }else{
            self.$el.html(self.template({profile: self.model, profile_button: "Follow", num_publications: self.collection_postList.length, num_followers: self.collection_followersList.length, num_following: self.collection_followingList.length}))
            boto_perfil = self.$el.find(".click_profile_button");
            boto_perfil.addClass("button_follow");
          }

          //Mostrem les imatges de l'usuari
          var galeria_imatges = self.$el.find(".galeria_imatges_usuari")
          var eb = self.eventBus
          self.collection_postList.each(function(post) {
            var item = new PostItemView({eventBus: eb, model: post}).render().el
            galeria_imatges.append(item)
          })

        }).catch(function(err) {
          console.log(`Error: ${err}`)
        });
        // -- FI Obtenir si l'usuari 'user' està dins els seguidors de l'usuari que té la sessió --
      }
      return this
    }
})

module.exports = UserProfile