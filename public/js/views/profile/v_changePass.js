/**
 * Created by moises on 27/04/2017.
 */
var t_changePass = require("raw-loader!../../../templates/profile/t_changePass.html")
var bcrypt = require('bcrypt-nodejs');

var changePass = Backbone.View.extend({

    className: 'container',

    initialize: function(params) {
        this.eventBus = params.eventBus;
        this.template = _.template(t_changePass);
        this.localEventBus = _.extend({}, Backbone.Events) //EventBus local per rebre els events de "click" de les subvistes (vistes que conté aquesta vista)
    },

    events: {
        'click  #btnChangePass' : 'click_change_pass',
        'click #btnCancelChangePass' : 'click_cancel_pass'
    },

    click_change_pass : function () {
        var self=this;
        if (($("#password1").val() == $("#password2").val()))  {
            this.model.set({
                'password': bcrypt.hashSync(this.$('#password1').val())
            });
            this.model.save(null, {
                success: function () {
                  self.eventBus.trigger('ui:switch:editProfile', self.model.get('username'));
                },
            });

        }else{
            alert( "Passwords must match" );
        }

    },

    click_cancel_pass : function (){
        this.eventBus.trigger('ui:switch:editProfile', this.model.get('username'));
    },

    render: function () {
        this.$el.html(this.template({profile: this.model.toJSON()}))
        return this
    }

})

module.exports = changePass