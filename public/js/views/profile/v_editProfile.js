/**
 * Created by moises on 06/04/2017.
 */
var t_editProfile = require("raw-loader!../../../templates/profile/tl_editProfile.html")
var $ = jQuery = require('jquery');
var EditProfile = Backbone.View.extend({

    className: 'container',

    initialize: function(params) {
        this.eventBus = params.eventBus;
        this.template = _.template(t_editProfile);
        this.localEventBus = _.extend({}, Backbone.Events);//EventBus local per rebre els events de "click" de les subvistes (vistes que conté aquesta vista)
    },

    events: {
        'click #btnSaveProfile' : 'click_save_profile',
        'click #btnGoChangePass' : 'click_go_change_pass',
        'click #btnCancelEditProfile' : 'click_cancel'
    },

    click_go_change_pass : function () {
        this.eventBus.trigger('ui:switch:goChangePass', this.model.get('username'));
    },

    click_cancel : function (){
        this.eventBus.trigger('ui:switch:profile', this.model.get('username'));
    },

    click_save_profile: function () {
        var self=this;
        this.model.set({
            'name' : this.$('#firstname').val(),
            'lastName' :this.$('#lastname').val(),
            'email' :this.$('#email').val(),
            'bio' :this.$('#bio').val(),
            'website' :this.$('#website').val(),
            'gender' : this.$('#gender').val()

        })
        this.model.save(null, {
            success: function () {
                //Si s'ha afegit una foto
                if (self.$el.find('#inputFile').prop('files') && self.$el.find('#inputFile').prop('files')[0] ){
                    self.eventBus.trigger('view:editProfile:uploadImage',self.$el.find('#inputFile').prop('files')[0],  self.model.toJSON().username )
                }else{
                    self.eventBus.trigger('ui:switch:profile', self.model.get('username'));
                }
            }
        }).catch(function(err) {
          console.log(`Error: ${err}`)
        });
    },

    render: function () {
        this.$el.html(this.template({profile: this.model}))
        this.$('#gender').val(this.model.toJSON().gender);
        var arrel = this;
        this.$('#inputFile').change(function () {
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    arrel.$('#imageProfile').attr('src', e.target.result);
                };
                reader.readAsDataURL(this.files[0]);
            }
        });
        return this
    }

})

module.exports = EditProfile