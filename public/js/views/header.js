var t_header = require("raw-loader!../../templates/header.html");
var UserProfile = require('./profile/v_profile');

var userData = {}

var Header = Backbone.View.extend({

  initialize: function (params) {
    this.eventBus = params.eventBus
    this.template = _.template(t_header)
    if (params.user)
      this.setUserData(params.user)

    var view = this
    params.eventBus.on('localstorage:set:user', function(user) {
      view.setUserData(user)
      view.render()
    })
  },

  events: {
      'click .icon-web': 'click_home',
      'click .icon-home': 'click_home',
      'click .icon-xats': 'click_xats',
      'click .icon-activity': 'click_activity',
      'click .icon-add': 'click_add',
      'click .icon-profile': 'click_profile',
      'click .icon-search': 'click_search',
      'click .icon-logout': 'click_logout',
  },

  click_home: function () {
    this.eventBus.trigger('ui:switch:home')
  },

  click_xats: function () {
    this.eventBus.trigger('ui:switch:xats')
  },

  click_activity: function () {
    this.eventBus.trigger('ui:switch:activity')
  },

  click_add: function () {
    this.eventBus.trigger('ui:switch:add')
  },

  click_profile: function () {
    //this.eventBus.trigger('ui:switch:profile')
    this.eventBus.trigger('ui:switch:profile', userData.username)
  },

  click_search: function () {
    this.eventBus.trigger('ui:switch:search')
  },

  click_logout: function () {
    this.eventBus.trigger('ui:switch:logout')
  },

  render: function () {
    this.$el.html(this.template({user: userData}))
    return this
  },

  setUserData: function (user) {
    userData = user
  }

})

module.exports = Header