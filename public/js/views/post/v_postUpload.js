var t_postUpload = require("raw-loader!../../../templates/post/t_postUpload.html");
var $ = jQuery = require('jquery');
var bootstrapImgUpload = require("bootstrap-imageupload");

var PostUpload = Backbone.View.extend({

    initialize: function (params) {
        this.eventBus = params.eventBus;

    },
    template: _.template(t_postUpload),

    className: 'container',

    events: {
        'click #video': 'videoTab',
        'click #image': 'imageTab',
        'click .btn-success': 'uploadPost',
        'change .imageupload': 'hideWarning',
        'change #fileInput2': 'previewVideo'
    },

    render: function () {
        this.$el.html(this.template());
        this.imageTab()
        this.$el.find('#imageuploader').imageupload({
            maxFileSizeKb: 4096,
            maxHeight: 500,
            maxWidth: 500
        });
        return this
    },

    uploadPost: function () {
        var $description = this.$el.find('#description').val();
        if (this.$el.find('.file-tab').is(":visible")) {
          if (this.$el.find('#fileInput').prop('files')[0]) {
            this.eventBus.trigger('view:post:upload', this.$el.find('#fileInput').prop('files')[0], $description, 'image');
          } else {
            this.$el.find('.alert-warning').show();
          }
        } else {
          if (this.$el.find('#fileInput2').prop('files')[0]) {
            this.eventBus.trigger('view:post:upload', this.$el.find('#fileInput2').prop('files')[0], $description, 'video');
          } else {
            this.$el.find('.alert-warning').show();
          }
        }
        return this
    },

    videoTab: function () {
        this.$el.find('#video').addClass("active");
        this.$el.find('#image').removeClass("active");
        this.$el.find('.file-tab').hide();
        this.$el.find('.video-tab').show();
    },

    imageTab: function () {
        this.$el.find('#video').removeClass("active");
        this.$el.find('#image').addClass("active");
        this.$el.find('.file-tab').show();
        this.$el.find('.video-tab').hide();
    },

    hideWarning: function () {
        this.$el.find('.alert-warning').hide();
    },

    previewVideo: function () {
      var reader = new FileReader();
      that = this
      reader.onload = function() {
        that.$el.find('.videoPreview').attr("src", reader.result).show();
      }
      reader.readAsDataURL(this.$el.find('#fileInput2').prop('files')[0]);

    }
});

module.exports = PostUpload;
