var tl_postItem = require("raw-loader!../../../templates/post/tl_postItem.html")

var PostItemView = Backbone.View.extend({

  initialize: function(params) {
    this.eventBus = params.eventBus;
    this.template = _.template(tl_postItem);
  },

  className: 'col-md-4',

  events: {
      'click .imatge_galeria': 'getPost'
  },

  getPost: function () {
    this.eventBus.trigger('view:post:get', this.model.attributes.postId)
  },

  render: function () {
    this.$el.html(this.template({post: this.model}))
    return this
  }

});

module.exports = PostItemView
