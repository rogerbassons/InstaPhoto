var tl_postDetail = require("raw-loader!../../../templates/post/tl_postDetail.html")

var PostDetailView = Backbone.View.extend({

  initialize: function(params) {
    this.eventBus = params.eventBus;
    this.template = _.template(tl_postDetail);
    this.model.set({userName: params.user}, {silent:true})
  },

  events: {
      'click .submitButton': 'addComment',
      'click .glyphicon-heart-empty': 'likePost',
      'click .glyphicon-heart': 'unLikePost',
      'keypress': 'submitEnter'
  },

  addComment: function () {
    this.eventBus.trigger('view:post:addComment', this.model.get('postId'), this.$('#comment').val())
  },

  likePost: function () {
    this.eventBus.trigger('view:post:like', this.model.get('postId'))
    this.$el.find('.glyphicon').addClass("btn-danger").addClass("glyphicon-heart").removeClass("btn-default").removeClass("glyphicon-heart-empty");
  },

  unLikePost: function () {
    this.eventBus.trigger('view:post:unLike', this.model.get('postId'))
    this.$el.find('.glyphicon').removeClass("btn-danger").removeClass("glyphicon-heart").addClass("btn-default").addClass("glyphicon-heart-empty");
  },

  submitEnter: function (e) {
    if (e.which === 13) {
      this.addComment()
    }
  },

  render: function () {
    this.$el.html(this.template({post: this.model}))
    this.delegateEvents()
    return this
  },

});

module.exports = PostDetailView
