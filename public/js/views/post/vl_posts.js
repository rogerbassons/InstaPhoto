var tl_posts = require("raw-loader!../../../templates/post/tl_posts.html")
var PostItemView = require("./vl_postItem")

var PostListView = Backbone.View.extend({

  initialize: function(params) {
    this.eventBus = params.eventBus;
    this.template = _.template(tl_posts);
  },

  className: 'container',

  render: function () {
    this.$el.html(this.template())
    var el = this.$el.find(".row")
    var eb = this.eventBus
    this.collection.each(function(post) {
      var item = new PostItemView({eventBus: eb, model: post}).render().el
      el.append(item)
    })
    return this
  }

});

module.exports = PostListView
