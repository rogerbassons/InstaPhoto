/**
 * Created by moises on 28/5/17.
 */

var t_chat = require("raw-loader!../../../templates/chat/tl_chat.html")
var t_chatMessage = require("raw-loader!../../../templates/chat/tl_chatMessage.html")
var t_chatReceiver = require("raw-loader!../../../templates/chat/tl_chatReceiver.html")
var t_chatUser = require("raw-loader!../../../templates/chat/tl_chatUsers.html")

var $ = jQuery = require('jquery');
//var io = require('socket.io');
var Message = require('../../chat/message')

var ChatView = Backbone.View.extend({

    className: 'container',
    socket: '',

    initialize: function (options) {
        this.eventBus = options.eventBus;
        this.messageCount = 0;
        this.username = options.username;
        this.receiver = 'everyone';
        this.messages = {};
        this.template = _.template(t_chat);


        var self = this;

        if (!this.socket) {

            this.socket = io.connect('', {
                query: 'username=' + this.username
            });
        }

        // keep track of chat messages
        this.socket.on('message', function (data) {

            data.position = 'left';

            // save message
            if (data.type != "info") {

                var user = '';
                if (data.receiver == 'everyone') {
                    user = 'everyone'
                } else {
                    user = data.username;
                }

                if(!self.messages[user]) {
                    self.messages[user] = [];
                }
                self.messages[user].push(data);

            } else {
                self.print(data);
            }

            // print message or show alert
            if (data.receiver == 'everyone')  {
                if (self.receiver == 'everyone') {
                    self.print(data);
                } else {
                    $('#everyone').find('.message-alert').addClass('visible');
                }

            } else {
                if (self.receiver == data.username) {
                    self.print(data);
                } else {
                    $('#'+data.username).find('.message-alert').addClass('visible');
                }
            }

            self.setScroll();

        });

        // keep track of users
        this.socket.on('users', function (data) {

            // remove current user
            data = _.reject(data, function (user) {
                return user.username === self.username;
            });

            $('menu.list-friends').html(self.usersTemplate({users: data}))
        });

    },

    events: {
        'click #send': 'sendMessage',
        'click .clear': 'clear',
        'click .logout': 'logout',
        'click .list-friends li': 'selectUser'
    },

    render: function () {
        this.$el.html(this.template({username: this.username}));

        this.socket.emit('getusers');
        this.selectUser({currentTarget : {id : 'everyone'}})
        return this;
    },


    // send message to the server
    sendMessage: function () {

        if ($('#input-message').val() == '') {
            return false;
        }

        var messageSender, senderClass, data, pattern, res;

        pattern = /(www\.)?([\w\-]+)\.([\w]+)/gm;
        res = pattern.test( $('#input-message').val());


        if (res) {
            res = /([a-zA-Z-0-9_]+)\.(jpg|png|gif)/.test( $('#input-message').val());
            if (res) {
                senderClass = new Message.ImageLinkMessageSender(this.socket, $('#input-message'));
            }
            else {
                senderClass = new Message.LinkMessageSender(this.socket, $('#input-message'));
            }
        }
        else {
            senderClass = new Message.TextMessageSender(this.socket, $('#input-message'));
        }

        messageSender = new Message.MessageSender(senderClass);


        var date = new Date();
        var time = pad(date.getHours()) + ':' + pad(date.getMinutes());

        function pad (value){
            return value.toString().length > 1 ? value : '0' + value;
        }


        data = {
            username: this.username,
            receiver: this.receiver,
            type: 'message',
            date: date,
            time: time
        };

        messageSender.send(data);

        // print message
        data.position = 'right';
        this.print(data);
        this.setScroll();

        // save message
        if(!this.messages[this.receiver]) {
            this.messages[this.receiver] = [];
        }
        this.messages[this.receiver].push(data);


        return false;
    },

    selectUser: function (event) {

        this.refresh();
        this.receiver = event.currentTarget.id;
        var img = [''];

        $('.list-friends li').removeClass('active');
        $('#'+this.receiver).addClass('active');

        if (this.receiver != 'everyone'){
            var $htmlDoc = $( event.currentTarget.innerHTML );
            img = $htmlDoc.text('img');
        }

        $('.receiver').html(this.receiverTemplate({
            receiver: this.receiver, img: img[0].currentSrc
        }));

        var conversation = this.messages[this.receiver];

        if (conversation) {
            var msgTmpl = '';

            for (var i = 0; i < conversation.length; i++) {
                msgTmpl += this.messageTemplate(conversation[i]);
            }

            this.messageCount = conversation.length;
            $('.count span').html(this.messageCount);
            $(".messages").append(msgTmpl);

            this.setScroll();
        }

        $('#'+this.receiver).find('.message-alert').removeClass('visible');
    },

    // print message to message box
    print: function (data) {
        this.messageCount++;
        $('.count span').html(this.messageCount);
        $(".messages").append(this.messageTemplate(data));
    },

    // clear message box
    refresh: function () {
        this.messageCount = 0;
        $('.count span').html(this.messageCount);
        $(".messages").html('');
    },

    // remove messages
    clear: function () {
        this.refresh();
        this.messages[this.receiver] = [];
    },

    // set scroll to bottom of message box
    setScroll: function () {
        var scroll = document.getElementById("messages").scrollHeight;
        $(".chat-body").scrollTop(scroll);
    },

    // disconnect user
    logout: function () {
        this.socket.disconnect();
        this.eventBus.trigger('ui:switch:closechat');
        this.undelegateEvents();
    },

    messageTemplate: _.template(t_chatMessage),

    usersTemplate: _.template(t_chatUser),

    receiverTemplate: _.template(t_chatReceiver)



});

module.exports = ChatView;