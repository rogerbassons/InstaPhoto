/**
 * Created by Moises on 19/04/2017.
 */
var profile = Backbone.Model.extend({
    url: "/api/users/self"
});
// Return the model for the module
module.exports = profile;