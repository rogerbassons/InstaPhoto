/**
 * @author: Robert Garcia Ventura
 * @date: 20/05/2017
 */

var FollowingModel = Backbone.Model.extend({
  urlRoot: "/api/users/self/follow",

  initialize: function(params) {
    switch (params.tipusRelacio){
      case "Following":
        this.urlRoot = "/api/users/" + params.username + "/unfollow";
        break;
      case "Follow":
        this.urlRoot = "/api/users/" + params.username + "/follow";
        break;
      default:

        break;
    }
  }
});
// Return the model for the module
module.exports = FollowingModel;
