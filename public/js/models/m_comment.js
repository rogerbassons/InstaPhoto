var CommentModel = Backbone.Model.extend({
  urlRoot: "/api/comments"
});
// Return the model for the module
module.exports = CommentModel;
