var LikeModel = Backbone.Model.extend({
  urlRoot: "/api/likes"
});
// Return the model for the module
module.exports = LikeModel;
