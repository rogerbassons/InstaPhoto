module.exports = function(app) {

  var express = require('express')

  var Posts = require('../controllers/c_posts')(app)
  var Comments = require('../controllers/c_comments')(app)
  var util = require('../util')
  var User = require('../controllers/c_user')(app)
  var Likes = require('../controllers/c_likes')(app)

  var router = express.Router()

  router.get('/api/users/self', User.getUserSelf, util.sendAuthError)
  router.put('/api/users/self', User.updateUser, util.sendAuthError )
  router.get('/api/users/search', User.searchUsers, util.sendAuthError)
  router.get('/api/users/:userId', User.getUserDetail, util.sendAuthError)
  router.put('/api/users/:userId', User.updateUser, util.sendAuthError)

  router.get('/api/users/self/feed', Posts.getUserFeed, util.sendAuthError)
  router.get('/api/users/self/posts', Posts.getPostsSelf, util.sendAuthError)
  router.get('/api/users/:userId/posts', Posts.getPosts, util.sendAuthError)

  router.get('/api/posts/:postId', Posts.getPostDetail, util.sendAuthError)
  router.post('/api/posts/:postId/comments', Comments.create, util.sendAuthError)
  router.post('/api/posts/:postId/likes', Likes.create, util.sendAuthError)
  router.post('/api/posts/:postId/unlike', Likes.remove, util.sendAuthError)

  router.get('/api/users/self/followers', User.getFollowersSelf, util.sendAuthError)
  router.get('/api/users/:userId/followers', User.getFollowers, util.sendAuthError)

  router.get('/api/users/self/followings', User.getFollowingsSelf, util.sendAuthError)
  router.get('/api/users/:userId/followings', User.getFollowings, util.sendAuthError)

  router.post('/api/users/:userId/follow', User.followUser, util.sendAuthError)
  router.post('/api/users/:userId/unfollow', User.unfollowUser, util.sendAuthError)

  router.post('/api/media', Posts.create, util.sendAuthError)
  router.post('/api/users/self/image', User.updateImage, util.sendAuthError)

  return router
}
