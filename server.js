var express = require('express');
var http = require('http');
var path = require('path');
var cors = require('cors');
var bodyParser = require('body-parser');
var favicon = require('serve-favicon');
var morgan = require('morgan');
var methodOverride = require('method-override');
var serveStatic = require('serve-static');
var session = require('express-session');
var bcrypt = require('bcrypt-nodejs');

var app = express();

// Application configuration
app.set('port', process.env.PORT || 3000);

app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(function (error, req, res, next) {
  error.location = "ERROR_JSON_PARSE";
  error.code = 400;
  next(error);
});
app.use(methodOverride());
app.use(cors());
app.use(session({
  secret: '63?gdº93!6dg36dºb36%Vv57V%c$%/(!V497',
  resave: true,
  saveUninitialized: true
}));

app.use(serveStatic(path.join(__dirname, 'public'), {'index': ['index.html', 'index.htm']}));
app.use("/uploads", express.static(__dirname + '/uploads'));

app.use(function (err, req, res, next) {
  var code = err.code || 500;
  var json = {};
  json.type = err.type || "ERROR_UNKNOWN";
  json.name = err.name || "UNKNOWN";
  json.message = err.message || "Unknown error";
  json.stack = err.stack || "No stack trace available";
  res.status(code).send({
    error: json
  });
});


// Chat initialization
var Chat = require('./controllers/c_chat')


// Database initialization

app.db = require('./models');

var jo
app.db.init(app.get('env'))
  .then(function() {
    return app.db.User.create({name: 'Emmanuel', lastName: 'Amunike', password: bcrypt.hashSync('jo'), username: 'jo', bio : 'Soc un ex-jugador negre', website : 'www.amunike.com', imatgePerfil:'./uploads/img-default.png', gender : 'Female', email: 'manu@hotmail.com'})
  })
  .then(function(user) {
    jo = user
    app.db.Post.create({postId: 1, caption: 'wtf is this shit', url: 'https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/SMPTE_Color_Bars.svg/329px-SMPTE_Color_Bars.svg.png', type: 'image'})
      .then(function (post) {
        app.db.Comment.create({text: 'testtttttt'})
          .then(function (comment) {
            comment.setPost(post)
            comment.setUser(user)
            app.db.Like.create(user)
              .then(function (like) {
                like.setPost(post)
                like.setUser(user)
              })
          })
      return post.setUser(user)
    })
    app.db.Post.create({postId: 2, caption: 'Test', url: 'http://i.imgur.com/JIBIjcH.jpg', type: 'image'})
    .then(function(post) {
      return post.setUser(user)
    })
    app.db.Post.create({postId: 3, caption: 'Quina platja', url: 'https://cdn.shopify.com/s/files/1/0426/9481/files/moonstrucktraveller-frankies-bikinis-stella-top_1024x1024.jpg?v=1482789280', type: 'image'})
    .then(function(post) {
      return post.setUser(user)
    })
    app.db.Post.create({postId: 4, caption: 'Que cau', url: 'https://s-media-cache-ak0.pinimg.com/originals/da/ee/f3/daeef34471e0dcfe9ac295b7b5a8f5be.png', type: 'image'})
    .then(function(post) {
      return post.setUser(user)
    })
    app.db.Post.create({postId: 8, caption: 'Test', url: 'http://www.sample-videos.com/video/mp4/480/big_buck_bunny_480p_1mb.mp4', type: 'video'})
      .then(function(post) {
        return post.setUser(user)
      })
    return app.db.Post.create({postId: 5, caption: 'Que passa', url: 'https://upload.wikimedia.org/wikipedia/en/2/24/Lenna.png', type: 'image'})
    .then(function(post) {
      return post.setUser(user)
    })
    return user
  })
  .then(function() {
    return app.db.User.create({name: 'Test1', lastName: 'ReTest1', password: bcrypt.hashSync('test1'), username: 'test1', bio : 'Soc un test1', website : 'www.test1.com', gender : 'Male', email: 'test1@mtalia.tk', imatgePerfil:'https://i.kinja-img.com/gawker-media/image/upload/s--BO15avLg--/c_scale,f_auto,fl_progressive,q_80,w_800/18mp1e7vpadplgif.gif'})
  })
  .then(function(user) {
    app.db.Post.create({postId: 6, caption: 'test1', url: 'https://raw.githubusercontent.com/plu/JPSimulatorHacks/master/Data/test.png', type: 'image'})
    .then(function(post) { 
      return post.setUser(user)
    })
      jo.addFollowing(user)
      return user
  })
.then(function() {
    return app.db.User.create({name: 'Test2', lastName: 'ReTest2', password: bcrypt.hashSync('test2'), username: 'test2', bio : 'Soc un test2', website : 'www.test2.com', gender : 'Male', email: 'test2@mtalia.tk', imatgePerfil:'https://s-media-cache-ak0.pinimg.com/736x/73/db/97/73db97c0c4a9c9b009d69f21ea48ecdc.jpg',})
  })
  .then(function(user) {
    app.db.Post.create({postId: 7, caption: 'test2', url: 'http://www.estilosdeaprendizaje.org/testestilosdeaprendizaje.png', type: 'image'})
    .then(function(post) { 
      return post.setUser(user)
    })
      jo.addFollowing(user)
	    user.addFollowing(jo)
      return user
  })
  .then(function () {
    app.use(require('./routers/noAuthRouter')(app));
    app.use(require('./routers/authRouter')(app));

    var port = process.env.OPENSHIFT_NODEJS_PORT || app.get('port');
    var ip = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";

    var server = http.createServer(app).listen(port, ip, function () {
      console.log("Express server listening on " + ip + ":" + port);
    })
    Chat.init(server, app);



  })
  .catch(function (err) {
    console.log("Error initializing database: " + err);
  })
  .done();
